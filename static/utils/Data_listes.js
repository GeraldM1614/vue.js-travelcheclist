
 export default 
     [{
         name: 'Preparatifs',
         icon: 'receipt',
         id: '23234rf3gf',
          todo: [
            {id: '13f3f31f3', name: 'assurances voyages', checked: false},
            {id: '32f134f13', name: 'assurances annulation', checked: true}
          ]
       },
       {
         name: 'Les medicaments',
         icon: 'local_pharmacy',
         id: 'qwefwef3f',
          todo: [
            {id: 'qwfwef34f34f', name: 'comprimes contre la douleur', checked: false},
            {id: 'qwef1332wqec', name: 'comprimes contre la paludisme', checked: true},
            {id: 'qwf323qfcwqec', name: 'Creme antibiotique (brulure, coupure)', checked: false}
          ]
       },
          {
            name: 'Articles de toilette',
            icon: 'business_center',
            id: 'qwecqc3',
            todo: [
              {id: 'qwevc3c3q', name: 'savon a lessive', checked: false},
              {id: 'qwvwevq3f', name:'Deodorant', checked: false}
            ]
          }      
        ]
    
