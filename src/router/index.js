import Vue from 'vue'
import Router from 'vue-router'
import ListTodo from '@/components/ListeTodo.vue';
import Restore from '@/components/Restore.vue';
import Nouveautes from '@/components/Nouveautes.vue';


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'ListTodo', component: ListTodo},
    { path: '/restaurer', name:'Restore', component: Restore},
    { path: '/nouveautes',name:'Nouveautes', component: Nouveautes}
  ]
})
