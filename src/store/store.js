import Vue from 'vue';
import Vuex from 'vuex';
// import VuexPersistence from 'vuex-persist'

import listeTodo from './modules/listeTodo';
import listeArchive from './modules/listeArchive';
import listeNouveautes from './modules/listeNouveautes';
Vue.use(Vuex);

// const vuexLocal = new VuexPersistence({
//   storage: window.localStorage
// })

export const store = new Vuex.Store({
  modules: {
    listeTodo,
    listeArchive,
    listeNouveautes
  },
//  plugins: [vuexLocal.plugin]
})