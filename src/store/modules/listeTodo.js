
const uuidv1 = require('uuid/v1');

const state = {
  listeTodo:[]
};
const mutations = {
  'INITIAL_SET_LIST' (state, listeTodo) {
    state.listeTodo = listeTodo;
  },
  'ADD_TODO' (state, {listId, todo}) {
    state.listeTodo[state.listeTodo.findIndex(x => x.id === listId)].todo.push(todo)
  },
  'REMOVE_TODO' (state, {listId, todoId}) {
    const liste = state.listeTodo[state.listeTodo.findIndex(x => x.id === listId)]
    liste.todo.splice(liste.todo.findIndex(x => x.id === todoId), 1)
  },
  'ADD_LISTE' (state, payload) {
    payload.id = uuidv1();
    state.listeTodo.push(payload)
  },
  'REMOVE_LISTE_TODO' (state, listId) {
    state.listeTodo.splice(state.listeTodo.findIndex(x => x.id === listId), 1);
  },
  'RETRIEVE_LISTE_TODO' (state, payload) {
    state.listeTodo.push(payload);
  },
  'RETRIEVE_TODO' (state, {listId, todo}) {
    state.listeTodo[state.listeTodo.findIndex(x => x.id === listId)].todo.push(todo);
  }
};
const actions = {
  initListe: ({commit}, listeTodo) => {
    commit('INITIAL_SET_LIST', listeTodo);
  },

  addNewTodo: ({commit}, payload) => {
    commit('ADD_TODO',payload);
  },
  removeTodo: ({commit}, payload) => {
    commit('REMOVE_TODO', payload);
  },
  addNewListe: ({commit}, payload) => {
    commit('ADD_LISTE', payload)
  },
  removeListeTodo: ({commit}, payload) => {
    commit('REMOVE_LISTE_TODO', payload)
  },
  retrieveListeTodo: ({commit}, payload) => {
    commit('RETRIEVE_LISTE_TODO', payload)
  },
  retrieveTodo: ({commit}, payload) => {
    commit('RETRIEVE_TODO', payload)
  }
};
const getters = {
  getListeTodo: state => {
    return state.listeTodo;
  }
};
export default {
  state,
  mutations,
  actions,
  getters
}
