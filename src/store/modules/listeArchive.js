const state = {
  listeArchive:[]
};

const mutations = {
  'ADD_LISTE_ARCHIVE' (state, payload) {
    state.listeArchive.push(payload);
  },
  'ADD_TODO_ARCHIVE' (state, {listId, todo}) {
    state.listeArchive[state.listeArchive.findIndex(x => x.id === listId)].todo.push(todo);
  },
  'REMOVE_LISTE_ARCHIVE' (state, listId) {
    state.listeArchive.splice(state.listeArchive.findIndex(x => x.id === listId), 1);
  },
  'REMOVE_TODO_ARCHIVE' (state, {listId, todoId}) {
    const liste = state.listeArchive[state.listeArchive.findIndex(x => x.id === listId)]
    liste.todo.splice(liste.todo.findIndex(x => x.id === todoId), 1)
  }
};
const actions = {
  addNewListeArchive: ({commit}, payload) => {
    commit('ADD_LISTE_ARCHIVE', payload)
  },
  addNewTodoArchive: ({commit}, payload) => {
    commit('ADD_TODO_ARCHIVE', payload)
  },
  removeListeArchive: ({commit}, payload) => {
    commit('REMOVE_LISTE_ARCHIVE', payload)
  },
  removeTodoArchive: ({commit}, payload) => {
    commit('REMOVE_TODO_ARCHIVE',  payload)
  }
};
const getters = {
  getListeArchive: state => {
    return state.listeArchive;
  }
};
export default {
  state,
  mutations,
  actions,
  getters
}
