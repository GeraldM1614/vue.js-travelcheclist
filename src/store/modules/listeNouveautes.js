import listeNouveautes from '../../../static/utils/Data_newListe';

const state = {
  listeNouveautes:[]
};

const mutations = {
  'ADD_LISTE_NOUVEAUTES' (state, listeNouveautes) {
    state.listeNouveautes = listeNouveautes;
  },
  'REMOVE_LISTE_NOUVEAUTES' (state, listId) {
    state.listeNouveautes.splice(state.listeNouveautes.findIndex(x => x.id === listId), 1);
  },
  'REMOVE_TODO_NOUVEAUTES' (state, {listId, todoId}) {
    const liste = state.listeNouveautes[state.listeNouveautes.findIndex(x => x.id === listId)]
    liste.todo.splice(liste.todo.findIndex(x => x.id === todoId), 1)
  }
};
const actions = {
  addNewListeNouveautes: ({commit}) => {
    commit('ADD_LISTE_NOUVEAUTES', listeNouveautes)
  },
  removeListeNouveautes: ({commit}, payload) => {
    commit('REMOVE_LISTE_NOUVEAUTES', payload)
  },
  removeTodoNouveautes: ({commit}, payload) => {
    commit('REMOVE_TODO_NOUVEAUTES',  payload)
  }
};
const getters = {
  getListeNouveautes: state => {
    return state.listeNouveautes;
  }
};
export default {
  state,
  mutations,
  actions,
  getters
}
